package com.example.finalexam

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

class InfoActivity : AppCompatActivity() {

    private lateinit var ramdenjeracrili: EditText
    private lateinit var ramdenjergadatanili: EditText
    private lateinit var martivadrturtulad: EditText
    private lateinit var buttoninfosave: Button

    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseDatabase.getInstance().getReference("CreatedUsers")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        init()
        registerlistener()

    }

    private fun init(){

        ramdenjeracrili = findViewById(R.id.ramdenjeracrili)
        ramdenjergadatanili = findViewById(R.id.ramdenjergadatanili)
        martivadrturtulad = findViewById(R.id.martivadrturtulad)
        buttoninfosave = findViewById(R.id.buttoninfosave)

    }

    private fun registerlistener(){

        buttoninfosave.setOnClickListener {
            startActivity(Intent(this, MenuActivity::class.java))
            val acrili = ramdenjeracrili.text.toString()
            val gadatanili = ramdenjergadatanili.text.toString()
            val martrtul = martivadrturtulad.text.toString()
            val createdUsers = CreatedUsers(acrili, gadatanili, martrtul)
            db.child(auth.currentUser?.uid!!).setValue(createdUsers)
        }


    }

}