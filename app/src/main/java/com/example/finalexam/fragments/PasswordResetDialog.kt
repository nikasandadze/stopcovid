package com.example.finalexam.fragments

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.example.finalexam.R
import com.google.firebase.auth.FirebaseAuth

class PasswordResetDialog(context: Context): Dialog(context) {

    private lateinit var editTextEmailAddrReset: EditText
    private lateinit var buttonPassReset : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.passreset_dialog)
        init()
        registerlistener()
    }
    private fun init(){
        editTextEmailAddrReset = findViewById(R.id.editTextEmailAddrReset)
        buttonPassReset = findViewById(R.id.buttonPassReset)
    }
    private fun registerlistener(){

        buttonPassReset.setOnClickListener {
            val email = editTextEmailAddrReset.text.toString()

            when {

                email.isEmpty() -> {
                    return@setOnClickListener
                }

            }

            FirebaseAuth.getInstance()
                .sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        dismiss()
                    }
                }
        }
    }
}