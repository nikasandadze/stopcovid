package com.example.finalexam

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.finalexam.fragments.RegisterDialog
import com.example.finalexam.fragments.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {
    private lateinit var editTextPass: EditText
    private lateinit var editTextEmailAddr: EditText
    private lateinit var btnLog: Button
    private lateinit var btnReg: Button
    private lateinit var btnForgPass: Button



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        registerListener()

        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        val navController = findNavController(R.id.fragmentContainerView)

        bottomNavigationView.setupWithNavController(navController)

    }

    private fun init(){

        editTextEmailAddr = findViewById(R.id.editTextEmailAddr)
        btnLog = findViewById(R.id.btnLog)
        editTextPass = findViewById(R.id.editTextPass)
        btnReg = findViewById(R.id.btnReg)
        btnForgPass = findViewById(R.id.btnForgPass)
    }
    private fun registerListener() {
        btnReg.setOnClickListener {
            val dialog = RegisterDialog(this)
            dialog.show()
        }
        btnForgPass.setOnClickListener {
            val dialog = PasswordResetDialog(this)
            dialog.show()
        }
        btnLog.setOnClickListener {
            val email = editTextEmailAddr.text.toString()
            val password = editTextPass.text.toString()

            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Empty!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        startActivity(Intent(this, InfoActivity::class.java))
                    }else{
                        Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                    }
                }
        }


    }


}